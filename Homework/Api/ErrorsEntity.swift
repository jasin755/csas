//
//  ErrorEntity.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 11/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation

struct ErrorsEntity: Decodable, CustomNSError {
    
    let status: String
    let errors: [ErrorEntity]

}
