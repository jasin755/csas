//
//  ApiError.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 11/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation

enum ApiError: Error {
    case jsonParseError(message: String)
    case error(message: String)
    case encodingError(message: String)
    case unauthorized(Data?)
}
