//
//  AnyDecodable.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 11/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation

enum AnyDecodable: Decodable, CustomStringConvertible {
    
    case string(String)
    case int(Int)
    case date(Date)
    
    var description: String {
        switch self {
        case .string(let string):
            return string
        case .int(let int):
            return "\(int)"
        case .date(let date):
            return date.description //TODO: Neni idealni
        }
    }
    
    init(from decoder: Decoder) throws {
        var container = try decoder.unkeyedContainer()
                
        if let string = try? container.decode(String.self) {
            self = .string(string)
            return
        }
        
        if let int = try? container.decode(Int.self) {
            self = .int(int)
            return
        }
        
        if let date = try? container.decode(Date.self) {
            self = .date(date)
            return
        }
        
        throw ApiError.jsonParseError(message: "Cannot decode AnyDecodable")
    }
    
}
