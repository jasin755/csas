//
//  Api.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 11/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

class Api {
    
    enum Event<T> {
        case loading
        case loaded(T)
        case error(Error)
        
        var loaded: T? {
            switch self {
            case .loaded(let value):
                return value
            default:
                return nil
            }
        }
    }
    
    private let baseUrl: URL
    private let requestQueue = DispatchQueue(label: "com.csas.response-queue", qos: .background, attributes: [.concurrent])
    private let session: Session
    
    init(baseUrl: URL) {
        self.baseUrl = baseUrl
        self.session = Session()
        self.session.session.configuration.requestCachePolicy = .returnCacheDataElseLoad
        self.session.session.configuration.urlCache = URLCache.shared
        self.session.session.configuration.httpCookieStorage = HTTPCookieStorage.shared
    }
    
    private func createAbsoluteUrl(with endPoint: String) -> URL {
        return baseUrl.appendingPathComponent(endPoint)
    }
    
    private func createBasicHeaders() -> HTTPHeaders {
        return ["WEB-API-key": "4d2c7469-b707-43af-8fdb-c985e419dc01"]
    }
    
    public func nextPage<T: IPaginated>(of object: IPaginated) -> Observable<Event<T>> {
        if let nextPage = object.nextPage {
            
            return Observable<Event<T>>.create { [weak self] (observer) -> Disposable in
                guard let self = self else { return Disposables.create() }
                
                observer.onNext(Event.loading)
                
                var params = object.dataForPaging.params ?? [String: Any]()
                params["page"] = nextPage
                
                let request = self.session.request(object.dataForPaging.url, method: object.dataForPaging.method, parameters: params, encoding: JSONEncoding(), headers: self.createBasicHeaders()) //TODO presunout do interceptoru
                    .responseData(queue: self.requestQueue) { (dataResponse) in
                        self.processResponse(dataResponse: dataResponse, observer: observer) { data in
                            Swift.Result(catching: { try T.decode(data: data, resource: object.dataForPaging.url, method: object.dataForPaging.method, params: object.dataForPaging.params) })
                        }
                    }

                request.resume()
                
                return Disposables.create {
                    request.cancel()
                }
                
            }
            
        }
        
        return .empty()
    }
    
    public func requestPaging<T: IPaginated>(endPoint: String, method: HTTPMethod = .get, params: [String: Any]? = nil) -> Observable<Event<T>> {
        
        let url = createAbsoluteUrl(with: endPoint)
        
        return Observable<Event<T>>.create { [weak self] (observer) -> Disposable in
            guard let self = self else { return Disposables.create() }
            
            observer.onNext(Event.loading)
            
            let request = self.session.request(url, method: method, parameters: params, encoding: JSONEncoding(), headers: self.createBasicHeaders()) //TODO presunout do interceptoru
                .responseData(queue: self.requestQueue) { (dataResponse) in
                    self.processResponse(dataResponse: dataResponse, observer: observer) { data in
                        Swift.Result(catching: { try T.decode(data: data, resource: url, method: method, params: params) })
                    }
                }

            request.resume()
            
            return Disposables.create {
                request.cancel()
            }
            
        }
    }
        
    public func request<T: Decodable>(endPoint: String, method: HTTPMethod = .get, params: [String: Any]? = nil) -> Observable<Event<T>> {
        
        let url = createAbsoluteUrl(with: endPoint)
        
        return Observable<Event<T>>.create { [weak self] (observer) -> Disposable in
            
            guard let self = self else { return Disposables.create() }
            
            observer.onNext(Event.loading)

            let request = self.session.request(url, method: method, parameters: params, encoding: JSONEncoding(), headers: self.createBasicHeaders()) //TODO presunout do interceptoru
                .responseData(queue: self.requestQueue) { (dataResponse) in
                    self.processResponse(dataResponse: dataResponse, observer: observer) { data in
                        Swift.Result(catching: { try T.decode(data: data) })
                    }
                }

            request.resume()
            
            return Disposables.create {
                request.cancel()
            }
        }
    }
        
    private func processResponse<T>(dataResponse: AFDataResponse<Data>, observer: AnyObserver<Event<T>>, decoder: @escaping (Data) -> Swift.Result<T, Error>) {
        do {
            let statusCode = dataResponse.response?.statusCode
            
            switch dataResponse.result {
            case .success(let data):
                switch statusCode {
                case .some(let statusCode) where statusCode == 401:
                    observer.onError(ApiError.unauthorized(data))
                case .some(let statusCode) where statusCode < 200 || statusCode >= 300:
                    let error = try ErrorsEntity.decode(data: data)
                    observer.onNext(Event.error(error))

                default:
                    let item = try decoder(data).get()
                    observer.onNext(Event.loaded(item))
                }
            case .failure(let error):
                observer.onNext(Event.error(error))
            }
        } catch {
            observer.onNext(Event.error(error))
        }
        observer.onCompleted()
    }
}
