//
//  ErrorEntity.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 11/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation

struct ErrorEntity: Decodable, CustomNSError {
    
    enum CodingKeys : String, CodingKey {
        case error, scope, parameters
        case transactionId = "cz-transactionId"
    }
    
    let error: String
    let scope: String?
    let parameters: [String: AnyDecodable]?
    let transactionId: String?
    
}


