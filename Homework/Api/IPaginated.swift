//
//  IPaginated.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 11/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation
import Alamofire

private let fakeJsonKeyName = "dataForPaging"

struct DataForPaging: Decodable {
    let url: URL
    let params: [String: String]?
    let method: HTTPMethod
}

protocol IPaginated : Decodable {

    var dataForPaging: DataForPaging { get }
    
    var pageNumber: Int { get }
    var pageSize: Int { get }
    var pageCount: Int { get }
    var nextPage: Int? { get }
    var recordCount: Int {get }

}


extension Decodable where Self: IPaginated {
    static func decode(data: Data, resource: URL, method: HTTPMethod, params: [String: Any]?) throws -> Self {
        
        if var json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
            
            let stringParams = params?.mapValues{ "\($0)" }

            var fakeDict: [String: Any] = ["url": resource.absoluteString, "method": method.rawValue]
            
            if let stringParams = stringParams {
                fakeDict["params"] = stringParams
            }
            
            json[fakeJsonKeyName] = fakeDict
            
            let data = try JSONSerialization.data(withJSONObject: json, options: [])
            return try decode(data: data)
        }

        return try decode(data: data)
    }
}
