//
//  BalanceEntity.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 12/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation

struct BalanceEntity: Decodable, Equatable {
    
    let balanceWithoutPennies: Int
    let balancePennies: Int
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        
        let balance = try container.decode(Double.self)
        self.balanceWithoutPennies =  Int(balance)
        self.balancePennies = Int(balance.truncatingRemainder(dividingBy: 1)*100)
    }
    
}
