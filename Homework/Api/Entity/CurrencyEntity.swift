//
//  CurrencyEntity.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 12/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation

enum Currency: String, Decodable {
    case CZK
    
    var currencySymbol: String {
        switch self {
        case .CZK:
            return "Kč"
        }
    }
    
}
