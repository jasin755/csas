//
//  RootAssembler.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 11/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation
import Swinject
import SwinjectAutoregistration

class RootAssembler {
    
    static let instance = RootAssembler()
    
    let assembler: Assembler
    
    init() {
        let container = Container()
        RootAssembler.registerBaseDependencies(using: container)
        
        let baseAssembler = Assembler(container: container)
        self.assembler = RootAssembler.registerModules(using: baseAssembler)
    }
    
    static func registerBaseDependencies(using container: Container) {
        container.register(Resolver.self) { $0 }
        container.register(AppRouter.self, factory: AppRouter.init)
        container.register(Api.self) { _ in Api(baseUrl: URL(string: "https://webapi.developers.erstegroup.com/api/csas/public/sandbox/v3")!) }
    }
    
    static func registerModules(using assembler: Assembler) -> Assembler {
        assembler.withModules { (context) in
            context.register(AccountListModule.self)
            context.register(AccountDetailModule.self)
        }
    }
    
}

class ModuleRegistrationContext {
    fileprivate var assemblies = [Assembly]()
    fileprivate var resolves = [(Assembler) -> Void]()
    
    public func register<T: IModule>(_ module: T.Type) {
        assemblies.append(ModuleAssembly(module: module))
    
        resolves.append { assembler in
            _ = assembler.resolver.resolve(T.self)!
        }
    }
}

private class GlobalsAssembly: Assembly {

    let registrations: [(Container) -> Void]

    init(registrations: [(Container) -> Void]) {
        self.registrations = registrations
    }

    func assemble(container: Container) {
        for registration in registrations {
            registration(container)
        }
    }

    static func register<T>(global: T, name: String? = nil) -> (Container) -> Void {
        return { (container: Container) in
            container.register(T.self, name: name) { _ in
                return global
            }
        }
    }
}

extension Assembler {
    
    func withModules(_ register: (_ context: ModuleRegistrationContext) -> Void) -> Assembler {
        let context = ModuleRegistrationContext()
        register(context)
        
        let assembler = Assembler(context.assemblies, parent: self).registerSelf()
        
        for action in context.resolves {
            action(assembler)
        }
        
        return assembler
    }
    
    public func with(assemblies: Assembly..., defaultObjectScope: ObjectScope = .graph) -> Assembler {
           return Assembler(assemblies, parent: self, defaultObjectScope: defaultObjectScope)
       }

       public func with(assemblies: [Assembly], defaultObjectScope: ObjectScope = .graph) -> Assembler {
           return Assembler(assemblies, parent: self, defaultObjectScope: defaultObjectScope)
       }

       public func with<G1>(globals g1: G1) -> Assembler {
           var registrations = [(Container) -> Void]()

           registrations.append(GlobalsAssembly.register(global: g1))

           return with(assemblies: GlobalsAssembly(registrations: registrations)).registerSelf()//swiftlint:disable:this register_self
       }

       public func with<G1, G2>(globals g1: G1, _ g2: G2) -> Assembler {
           var registrations = [(Container) -> Void]()

           registrations.append(GlobalsAssembly.register(global: g1))
           registrations.append(GlobalsAssembly.register(global: g2))

           return with(assemblies: GlobalsAssembly(registrations: registrations)).registerSelf()//swiftlint:disable:this register_self
       }
    
}

extension Assembler {
    @discardableResult
    public func registerSelf() -> Assembler {
        self.apply(assembly: AssemblerSelf(assembler: self))
        return self
    }
}

private class AssemblerSelf: Assembly {
    private let assembler: Assembler
    init(assembler: Assembler) {
        self.assembler = assembler
    }

    func assemble(container: Container) {
        let assembler: Assembler = self.assembler
        container.register(Assembler.self) { _ in
            return assembler
        }
    }
}


class ModuleAssembly<T: IModule>: Assembly {
    let module: T.Type
    init(module: T.Type) {
        self.module = module
    }
    
    func assemble(container: Container) {
        container.autoregister(module.self, initializer: module.init).inObjectScope(.container)
    }
}
