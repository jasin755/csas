//
//  Decodable.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 11/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation

private let dateFormatter = ISODateFormatter()

private class ISODateFormatter: DateFormatter {

    override init() {
        super.init()

        calendar = Calendar(identifier: .iso8601)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func parse(from string: String) throws -> Date {

        dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        if let date = date(from: string) {
            return date
        }

        throw NSError(domain: "CSAS", code: 0, userInfo: [NSLocalizedDescriptionKey: "Failed to parse date from invalid format."])
    }

    override func string(from date: Date) -> String {
        dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        return super.string(from: date)
    }
}

extension Decodable {
    
    static func decode(data: Data) throws -> Self {
        let decoder = JSONDecoder()
            
        decoder.dateDecodingStrategy = .custom({ (decoder) -> Date in
            let container = try decoder.singleValueContainer()
            let dateStr = try container.decode(String.self)
            return try dateFormatter.parse(from: dateStr)
        })
        
        return try decoder.decode(Self.self, from: data)
    }
    
}
