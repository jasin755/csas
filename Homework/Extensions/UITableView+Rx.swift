//
//  UITableView+Rx.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 12/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

extension Reactive where Base: UITableView {
    
    func modelPressed<T>(_ modelType: T.Type) -> Observable<T> {

        let modelSelected: ControlEvent<T> = self.modelSelected(modelType)
        let modelDeselected: ControlEvent<T> = self.modelDeselected(modelType)

        return Observable.merge(modelSelected.asObservable(), modelDeselected.asObservable())
    }
}
