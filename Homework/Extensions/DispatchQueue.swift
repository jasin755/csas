//
//  DispatchQueue.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 11/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation

extension DispatchQueue {
    static func mainSyncSafe<T>(execute work: () throws -> T) rethrows -> T {
        
        if Thread.isMainThread {
            return try work()
        }
        
        return try DispatchQueue.main.sync { try work() }
    }
}
