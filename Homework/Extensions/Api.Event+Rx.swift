//
//  Api.Event+Rx.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 11/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation
import RxSwift
import RxSwiftExt

protocol ApiEventConvertible {
    associatedtype Data
    var value: Api.Event<Data> { get }
}

extension Api.Event: ApiEventConvertible {
    typealias Data = T
    
    var value: Api.Event<T> {
        return self
    }

}


extension ObservableType where Element: ApiEventConvertible, Element.Data: Any {
    
    func elements() -> Observable<Element.Data> {
        return map { event -> Element.Data? in
                return event.value.loaded
        }
        .unwrap()
    }
    
}
