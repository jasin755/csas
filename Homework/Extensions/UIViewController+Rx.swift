//
//  UIViewControlle+Rx.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 11/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation
import RxSwift

extension Reactive where Base: UIViewController {
    var viewDidLoad: Observable<Void> {
        return self.methodInvoked(#selector(Base.viewDidLoad)).map { _ in Void() }
    }
    
    var viewWillAppear: Observable<Bool> {
        return self.sentMessage(#selector(Base.viewWillAppear)).map { $0.first as? Bool ?? false }
    }

    var viewDidAppear: Observable<Bool> {
        return self.methodInvoked(#selector(Base.viewDidAppear)).map { $0.first as? Bool ?? false }
    }
    
    var viewWillDisappear: Observable<Bool> {
        return self.sentMessage(#selector(Base.viewWillDisappear)).map { $0.first as? Bool ?? false }
    }

    var viewDidDisappear: Observable<Bool> {
        return self.methodInvoked(#selector(Base.viewDidDisappear)).map { $0.first as? Bool ?? false }
    }
    
    var viewWillLayoutSubviews: Observable<Void> {
        return self.sentMessage(#selector(Base.viewWillLayoutSubviews)).map { _ in Void() }
    }

    var viewDidLayoutSubviews: Observable<Void> {
        return self.methodInvoked(#selector(Base.viewDidLayoutSubviews)).map { _ in Void() }
    }
}
