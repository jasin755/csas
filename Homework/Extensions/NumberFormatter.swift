//
//  NumberFormatter.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 12/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation


extension NumberFormatter {
    
    static func currencyString(from amount: Double, currencySymbol: String) -> String? {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.groupingSeparator = " "
        numberFormatter.currencySymbol = currencySymbol
        numberFormatter.currencyDecimalSeparator = ","
        numberFormatter.currencyGroupingSeparator = " "
        numberFormatter.locale = Locale(identifier: "cs_CZ")
        return numberFormatter.string(from: NSNumber(value: amount))
    }
    
}
