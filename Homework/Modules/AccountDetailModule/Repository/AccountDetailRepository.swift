//
//  AccountDetailRepository.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 12/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation
import RxSwift

class AccountDetailRepository {
    
    private let api: Api
    
    init(api: Api) {
        self.api = api
    }
    
    func getAccountDetail(accountNumber: String) -> Observable<Api.Event<AccountDetailEntity>> {
        return api.request(endPoint: "transparentAccounts/\(accountNumber)")
    }
    
    func getAccountTransactions(accountNumber: String) -> Observable<Api.Event<AccountsTransactionsEntity>>  {
        return api.requestPaging(endPoint: "transparentAccounts/\(accountNumber)/transactions")
    }
    
}
