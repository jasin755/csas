//
//  Subject.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 12/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation

struct Subject: Decodable, Equatable {
    
    let accountNumber: String
    let bankCode: String
    let iban: String
    let constantSymbol: String?
    let specificSymbol: String?
    let specificSymbolParty: String?
    let description: String?
    let name: String?
    
}
