//
//  AccountTransactionEntity.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 12/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation

struct AccountTransactionEntity: Decodable, Equatable {
    
    let dueDate: Date
    let typeDescription: String
    let sender: Subject
    let receiver: Subject
    let amount: AmountEntity
    
}
