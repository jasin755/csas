//
//  Amount.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 12/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation

struct AmountEntity: Decodable, Equatable {
    let value: Double
    let currency: Currency
}
