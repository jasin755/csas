//
//  AccountTransactionsEntity.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 12/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation

struct AccountsTransactionsEntity: IPaginated {
    
    let dataForPaging: DataForPaging
    let pageNumber: Int
    let pageSize: Int
    let pageCount: Int
    let nextPage: Int?
    let recordCount: Int
    let transactions: [AccountTransactionEntity]

}
