//
//  AccountDetailModule.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 12/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation
import UIKit
import Swinject
import RxSwift

class AccountDetailModule: IModule {
    
    class ModuleOutput {
        
        let viewController: UIViewController
        let share: Observable<String>
        
        init(viewController: AccountDetailViewController, context: Context) {
            self.viewController = viewController
            self.share = context.share.asObservable()
        }
        
        internal class Context {
            
            let accountNumber: String
            let share = PublishSubject<String>()
            
            init(accountNumber: String) {
                self.accountNumber = accountNumber
            }
        }
    }
    

    
    private let assembler: Assembler
    
    required init(assembler: Assembler) {
        self.assembler = assembler
            .with(assemblies: AccountDetailModuleAssembly())
    }
    
    func show(accountNumber: String) -> ModuleOutput {
        
        let context = ModuleOutput.Context(accountNumber: accountNumber)
        
        return assembler
            .with(globals: context)
            .with(assemblies: AccountDetailViewAssembly())
            .resolver.resolve(ModuleOutput.self)!
    }
    

}
