//
//  AccountDetailService.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 12/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation
import RxSwift

class AccountDetailService {
    
    private let repository: AccountDetailRepository
    
    init(repository: AccountDetailRepository) {
        self.repository = repository
    }
    
    func getAccountDetail(accountNumber: String) -> Observable<Api.Event<AccountDetailEntity>> {
        return repository.getAccountDetail(accountNumber: accountNumber)
    }
    
    func getTransactions(accountNumber: String) -> Observable<Api.Event<AccountsTransactionsEntity>> {
        return repository.getAccountTransactions(accountNumber: accountNumber)
    }
    
}
