//
//  AccountDetailViewAssembly.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 12/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation
import Swinject

class AccountDetailViewAssembly: Assembly {
    
    func assemble(container: Container) {
        container.autoregister(AccountDetailViewModel.self, initializer: AccountDetailViewModel.init)
        container.autoregister(AccountDetailViewController.self, initializer: AccountDetailViewController.init)
        container.autoregister(AccountDetailModule.ModuleOutput.self, initializer: AccountDetailModule.ModuleOutput.init)
    }
    
}
