//
//  AccountDetailViewController.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 12/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import ReusableKit
import RxDataSources
import SnapKit

class AccountDetailViewController: UIViewController {
    
    private struct Cells {
        static let header = ReusableCell<AccountHeaderCell>(nibName: "AccountHeaderCell")
        static let info = ReusableCell<AccountInfoCell>(nibName: "AccountInfoCell")
        static let menu = ReusableCell<MenuCell>(nibName: "MenuCell")
        static let transaction = ReusableCell<AccountTransactionCell>(nibName: "AccountTransactionCell")
        
        static func register(in tableView: UITableView) {
            tableView.register(header)
            tableView.register(info)
            tableView.register(menu)
            tableView.register(transaction)
        }
    }
    
    @IBOutlet
    private weak var tableView: UITableView! {
        didSet {
            Cells.register(in: tableView)
            
            tableView.rowHeight = UITableView.automaticDimension
            tableView.estimatedRowHeight = 150
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)

            viewModel.sections
                .drive(tableView.rx.items(dataSource: dataSource))
                .disposed(by: disposeBag)
            
        }
    }
    
    private let dataSource = RxTableViewSectionedAnimatedDataSource<AccountDetailViewModel.SectionModel>(animationConfiguration: AnimationConfiguration(insertAnimation: .automatic, reloadAnimation: .none, deleteAnimation: .automatic),configureCell: { _, tableView, indexPath, item in
        switch item {
        case .header(accountName: let accountName, balance: let balanceEntity, let currency):
            let cell = tableView.dequeue(Cells.header, for: indexPath)
            cell.set(accoutName: accountName, balance: balanceEntity, currency: currency)
            return cell
        case .infoItem(label: let label, value: let value):
            let cell = tableView.dequeue(Cells.info, for: indexPath)
            cell.set(label: label, value: value)
            return cell
        case .menu(infoButtonTapped: let infoButtonTapped, transationsButtonTapped: let transationsButtonTapped, isInfoActive: let isInfoActive):
            let cell = tableView.dequeue(Cells.menu, for: indexPath)
            cell.set(transactionsButtonTapped: transationsButtonTapped, infoButtonTapped: infoButtonTapped, isInfoActive: isInfoActive)
            return cell
        case .transtactionItem(transaction: let transaction):
            let cell = tableView.dequeue(Cells.transaction, for: indexPath)
            cell.set(transaction: transaction)
            return cell
        }
    })
    
    private let disposeBag = DisposeBag()
    private let viewModel: AccountDetailViewModel
    
    init(viewModel: AccountDetailViewModel) {
        self.viewModel = viewModel
        
        super.init(nibName: "AccountDetailViewController", bundle: nil)
        
        rx.viewDidLoad
            .bind(to: viewModel.didLoad)
            .disposed(by: disposeBag)
        
        viewModel.title
            .drive(rx.title)
            .disposed(by: disposeBag)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.8784313725, green: 0.01176470588, blue: 0.3568627451, alpha: 1)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)]
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)]
    }
}
