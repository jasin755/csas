//
//  AccountHeaderCell.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 12/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation
import UIKit

class AccountHeaderCell: UITableViewCell {
               
    @IBOutlet
    private weak var balanceWithoutPennies: UILabel!
       
    @IBOutlet
    private weak var balancePennies: UILabel!
       
    @IBOutlet
    private weak var currencySymbol: UILabel!
    
    func set(accoutName: String, balance: BalanceEntity, currency: Currency?) {
                
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        numberFormatter.groupingSeparator = " "
        
        balanceWithoutPennies.text = numberFormatter.string(from: NSNumber(value: balance.balanceWithoutPennies))
        balancePennies.text = "\(balance.balancePennies)"
        currencySymbol.text = currency?.currencySymbol ?? "?"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        prepareForReuse()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        balanceWithoutPennies.text = nil
        balancePennies.text = nil
        currencySymbol.text = nil
    }
    
}
