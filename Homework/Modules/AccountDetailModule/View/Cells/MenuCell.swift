//
//  MenuCell.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 12/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation
import  UIKit
import RxSwift
import RxCocoa

class MenuCell: UITableViewCell {
 
    private var disposeBag = DisposeBag()
    
    @IBOutlet
    private weak var infoButton: UIButton!
    
    @IBOutlet
    private weak var transactionsButton: UIButton!
    
    func set(transactionsButtonTapped: AnyObserver<Void>, infoButtonTapped: AnyObserver<Void>, isInfoActive: Bool) {
        
        infoButton.setTitleColor(isInfoActive ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) : #colorLiteral(red: 0.8676652908, green: 0.4975027442, blue: 0.6471118927, alpha: 1), for: .normal)
        transactionsButton.setTitleColor(!isInfoActive ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) : #colorLiteral(red: 0.8676652908, green: 0.4975027442, blue: 0.6471118927, alpha: 1), for: .normal)
        
        infoButton.rx.tap
            .bind(to: infoButtonTapped)
            .disposed(by: disposeBag)
        
        transactionsButton.rx.tap
            .bind(to: transactionsButtonTapped)
            .disposed(by: disposeBag)
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        disposeBag = DisposeBag()
    }
    
}
