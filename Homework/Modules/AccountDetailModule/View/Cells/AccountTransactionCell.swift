//
//  AccountTransactionCell.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 12/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation
import UIKit

class AccountTransactionCell: UITableViewCell {
    
    @IBOutlet
    private weak var dateLabel: UILabel!
    
    @IBOutlet
    private weak var typeDescription: UILabel!
    
    @IBOutlet
    private weak var secondDescription: UILabel!
    
    @IBOutlet
    private weak var amount: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        prepareForReuse()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        dateLabel.text = nil
        typeDescription.text = nil
        secondDescription.text = nil
        amount.text = nil
    }
    
    func set(transaction: AccountTransactionEntity) {
        dateLabel.text = transaction.dueDate.description(with: nil)
        typeDescription.text = transaction.typeDescription
        secondDescription.text = transaction.sender.description
        
        amount.text =  NumberFormatter.currencyString(from: transaction.amount.value, currencySymbol: transaction.amount.currency.currencySymbol)
    }
    
}
