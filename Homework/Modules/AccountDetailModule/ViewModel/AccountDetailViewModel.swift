//
//  AccountDetailViewModel.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 12/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxDataSources

class AccountDetailViewModel {
    
    typealias SectionModel = AnimatableSectionModel<Section, SectionItem>
    
    enum Section: IdentifiableType {
        case `default`
        
        var identity: String {
              return "default"
          }
    }
    
    enum SectionItem: IdentifiableType, Equatable {
   
        case header(accountName: String, balance: BalanceEntity, currency: Currency?)
        case menu(infoButtonTapped: AnyObserver<Void>, transationsButtonTapped: AnyObserver<Void>, isInfoActive: Bool)
        case transtactionItem(transaction: AccountTransactionEntity)
        case infoItem(label: String, value: String)
        
        var identity: String {
            switch self {
            case .header:
                return "header"
            case .menu:
                return "menu"
            case .transtactionItem:
                return UUID().uuidString // Because API contains duplicate transactions
            case .infoItem(label: let label, value: _):
                return "infoItem.\(label)"
            }
        }
        
        static func == (lhs: AccountDetailViewModel.SectionItem, rhs: AccountDetailViewModel.SectionItem) -> Bool {
            switch (lhs, rhs) {
            case (.header(accountName: let accountNameLhs, balance: let balanceLhs, currency: let currencyLhs), .header(accountName: let accountNameRhs, balance: let balanceRhs, currency: let currencyRhs)):
                return accountNameLhs == accountNameRhs
                    && balanceLhs == balanceRhs
                    && currencyLhs == currencyRhs
            case (.menu(infoButtonTapped: _, transationsButtonTapped: _, isInfoActive: let isInfoActiveLhs), .menu(infoButtonTapped: _, transationsButtonTapped: _, isInfoActive: let isInfoActiveRhs)):
                return isInfoActiveLhs == isInfoActiveRhs
            case (.transtactionItem(transaction: let transactionLhs), .transtactionItem(transaction: let transactionRhs)):
                return transactionLhs == transactionRhs
            case (.infoItem(label: let labelLhs, value: let valueLhs), .infoItem(label: let labelRhs, value: let valueRhs)):
                return labelLhs == labelRhs && valueLhs == valueRhs
            default:
                return false
            }
        }
    }
    
    private let service: AccountDetailService
    
    //inputs
    let didLoad = PublishSubject<Void>()
    
    //output
    let sections: Driver<[SectionModel]>
    let title: Driver<String>
    
    init(service: AccountDetailService, context: AccountDetailModule.ModuleOutput.Context) {
        self.service = service
        
        let accountDetailResponse = didLoad
            .mapTo(context.accountNumber)
            .flatMapLatest(service.getAccountDetail)
            .share(replay: 1, scope: .whileConnected)
        
        let accountDetail = accountDetailResponse
            .elements()
    
        title = accountDetail
            .map { $0.name }
            .asDriver(onErrorDriveWith: .empty())
        
        let infoButtonTapped = PublishSubject<Void>()
        let transactionsButtonTapped = PublishSubject<Void>()
        
        let isInfoButtoActive = Observable.merge(infoButtonTapped.mapTo(true),transactionsButtonTapped.mapTo(false))
            .startWith(true)

        let staticSection = Observable.combineLatest(accountDetail, isInfoButtoActive)
            .map { ($0, $1, infoButtonTapped, transactionsButtonTapped) }
            .map(AccountDetailViewModel.createStaticSection)
        
        let infoData = Observable.combineLatest(accountDetail, isInfoButtoActive)
            .filter { $1 }
            .map { $0.0 }
            .map(AccountDetailViewModel.createInfoSection)
            .startWith([])
        
        let data = isInfoButtoActive
            .flatMapLatest { $0 ? infoData : service.getTransactions(accountNumber: context.accountNumber)
                .elements()
                .map { $0.transactions }
                .map(AccountDetailViewModel.createTransactionsSection)
                .startWith([])
            }
            .share()
        
        sections = data
            .withLatestFrom(staticSection) { $1 + $0 }
            .map { [SectionModel(model: .default, items: $0)] }
            .asDriver(onErrorDriveWith: .empty())

    }
    
    private static func createStaticSection(detailEntity: AccountDetailEntity, isInfoButtonActive: Bool, infoButtonTapped: PublishSubject<Void>, transactionsButtonTapped: PublishSubject<Void>) -> [SectionItem] {
        var items = [SectionItem]()
        //header
        items.append(.header(accountName: detailEntity.name, balance: detailEntity.balance, currency: detailEntity.currency))
        
        //menu
        items.append(.menu(infoButtonTapped: infoButtonTapped.asObserver(), transationsButtonTapped: transactionsButtonTapped.asObserver(), isInfoActive: isInfoButtonActive))
        
        return items
    }
    
    private static func createInfoSection(detailEntity: AccountDetailEntity) -> [SectionItem] {
        var items = [SectionItem]()
        
        //info
        items.append(.infoItem(label: "Majitel účtu", value: detailEntity.name))
        items.append(.infoItem(label: "Číslo účtu", value: "\(detailEntity.accountNumber)/\(detailEntity.bankCode)"))
        items.append(.infoItem(label: "IBAN", value: detailEntity.iban))
        
        return items
    }
    
    private static func createTransactionsSection(transactions: [AccountTransactionEntity]) -> [SectionItem] {
        return transactions.map { SectionItem.transtactionItem(transaction: $0) }
    }
    
}
