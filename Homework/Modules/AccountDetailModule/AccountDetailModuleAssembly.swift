//
//  AccountDetailModuleAssembly.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 12/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation
import Swinject

class AccountDetailModuleAssembly: Assembly {
    
    func assemble(container: Container) {
        container.autoregister(AccountDetailRepository.self, initializer: AccountDetailRepository.init)
        container.autoregister(AccountDetailService.self, initializer: AccountDetailService.init)
    }
    
}
