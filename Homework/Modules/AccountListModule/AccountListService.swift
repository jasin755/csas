//
//  AccountListService.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 11/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation
import RxSwift

class AccountListService {
    
    private let repository: AccountListRepository
    
    init(repository: AccountListRepository) {
        self.repository = repository
    }
    
    func getAccountList() -> Observable<Api.Event<AccountsEntity>> {
        return repository.getAccountList()
    }
    
    func nextPage(of accounts: AccountsEntity) -> Observable<Api.Event<AccountsEntity>> {
        return repository.nextPage(of: accounts)
    }
    
}
