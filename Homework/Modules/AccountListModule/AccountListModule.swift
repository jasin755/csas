//
//  AccountListModule.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 11/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation
import Swinject
import RxSwift
import RxCocoa

class AccountListModule: IModule {
    
    class ModuleOutput {
        
        let viewController: UIViewController
        let showDetail: Driver<String>
        
        init(context: Context, viewController: AccountListViewController) {
            self.showDetail = context.showDetail.asDriver(onErrorDriveWith: .empty())
            self.viewController = viewController
        }
        
        internal class Context {
            let showDetail = PublishSubject<String>()
        }
    }
    
    private let assembler: Assembler
    
    required init(assembler: Assembler) {
        self.assembler = assembler
            .with(assemblies: AccountListModuleAssembly())
    }
    
    func show() -> ModuleOutput {
        
        let context = ModuleOutput.Context()
        
        return assembler
            .with(globals: context)
            .with(assemblies: AccountListViewAssembly())
            .resolver.resolve(ModuleOutput.self)!
    }
    

}
