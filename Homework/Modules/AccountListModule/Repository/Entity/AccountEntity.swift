//
//  AccountEntity.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 11/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation

struct AccountEntity: Decodable, Equatable {
        
    let accountNumber: String
    let bankCode: String
    let transparencyFrom: Date
    let transparencyTo: Date
    let publicationTo: Date
    let actualizationDate: Date
    let balance: BalanceEntity
    let currency: Currency?
    let name: String
    let iban: String
            
}
