//
//  AccountListRepository.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 11/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

class AccountListRepository {
    
    private let api: Api
    
    init(api: Api) {
        self.api = api
    }
    
    func getAccountList() -> Observable<Api.Event<AccountsEntity>> {
        return api.requestPaging(endPoint: "transparentAccounts", method: .get).debug()
    }
    
    func nextPage(of accounts: AccountsEntity) -> Observable<Api.Event<AccountsEntity>>  {
        return api.nextPage(of: accounts)
    }
    
}
