//
//  AccountListViewAssembly.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 11/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation
import Swinject

class AccountListViewAssembly: Assembly {
    
    func assemble(container: Container) {
        container.autoregister(AccountListViewModel.self, initializer: AccountListViewModel.init)
        container.autoregister(AccountListViewController.self, initializer: AccountListViewController.init)
        container.autoregister(AccountListModule.ModuleOutput.self, initializer: AccountListModule.ModuleOutput.init)
    }
    
}
