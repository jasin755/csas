//
//  AccountCell.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 11/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation
import UIKit

class AccountCell: UITableViewCell {
    
    @IBOutlet
    private weak var accountNumber: UILabel!
    
    @IBOutlet
    private weak var accountName: UILabel!
    
    @IBOutlet
    private weak var balanceWithoutPennies: UILabel!
    
    @IBOutlet
    private weak var balancePennies: UILabel!
    
    @IBOutlet
    private weak var currencySymbol: UILabel!
    
    @IBOutlet
    private weak var shareIcon: UIImageView!
    
    @IBOutlet
    private weak var containerView: UIView!
        
    func set(account: AccountEntity) {
        accountNumber.text = "\(account.accountNumber)/\(account.bankCode)"
        accountName.text = account.name
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        numberFormatter.groupingSeparator = " "
        
        balanceWithoutPennies.text = numberFormatter.string(from: NSNumber(value: account.balance.balanceWithoutPennies))
        balancePennies.text = "\(account.balance.balancePennies)"
        currencySymbol.text = account.currency?.currencySymbol ?? "?"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.layer.cornerRadius = 5
        
        prepareForReuse()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        accountNumber.text = nil
        accountName.text = nil
        balanceWithoutPennies.text = nil
        balancePennies.text = nil
        currencySymbol.text = nil

    }
    
}
