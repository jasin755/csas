//
//  AccountListViewController.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 11/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation
import UIKit
import ReusableKit
import RxSwift
import RxDataSources

class AccountListViewController: UIViewController {
    
    private struct Cells {
        static let account = ReusableCell<AccountCell>(nibName: "AccountCell")
        
        static func register(in tableView: UITableView) {
            tableView.register(account)
        }
    }
    
    @IBOutlet
    private weak var tableView: UITableView! {
        didSet {
            
            Cells.register(in: tableView)
            
            tableView.rowHeight = UITableView.automaticDimension
            tableView.estimatedRowHeight = 150
            tableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
            tableView.tableFooterView = UIView()
            
            tableView.rx
                .modelSelected(AccountEntity.self)
                .bind(to: viewModel.accountSelected)
                .disposed(by: disposeBag)
                
            viewModel.sections
                .drive(tableView.rx.items(dataSource: dataSource))
                .disposed(by: disposeBag)
            
        }
    }
    
    private let dataSource = RxTableViewSectionedAnimatedDataSource<AccountListViewModel.SectionModel>(configureCell: { _, tableView, indexPath, item in
        let cell = tableView.dequeue(Cells.account, for: indexPath)
        cell.set(account: item)
        return cell
    })
    
    private let disposeBag = DisposeBag()
    private let viewModel: AccountListViewModel
    
    init(viewModel: AccountListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "AccountListViewController", bundle: nil)
        
        rx.viewDidLoad
            .bind(to: viewModel.didLoad)
            .disposed(by: disposeBag)
        
        viewModel.screenTitle
            .drive(rx.title)
            .disposed(by: disposeBag)
    }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
                navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.1764705882, green: 0.3921568627, blue: 0.8117647059, alpha: 1)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = view.bounds
        gradientLayer.colors = [#colorLiteral(red: 0.1764705882, green: 0.3921568627, blue: 0.8117647059, alpha: 1).cgColor, #colorLiteral(red: 0.4745098039, green: 0.6431372549, blue: 0.8117647059, alpha: 1).cgColor]
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
            
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)]
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)]
    }
            
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
