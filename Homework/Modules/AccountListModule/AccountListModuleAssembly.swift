//
//  AccountListAssembler.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 11/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation
import Swinject

class AccountListModuleAssembly: Assembly {
    
    func assemble(container: Container) {
        container.autoregister(AccountListService.self, initializer: AccountListService.init)
        container.autoregister(AccountListRepository.self, initializer: AccountListRepository.init)
    }
    
}
