//
//  AccountListViewModel.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 11/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxDataSources

class AccountListViewModel {
    
    typealias SectionModel = AnimatableSectionModel<Section, AccountEntity>
    
    enum Section: IdentifiableType {
        case `default`
        
        var identity: String {
              return "default"
          }
    }
    
    private let service: AccountListService
    private let disposeBag = DisposeBag()
    
    //inputs
    let didLoad = PublishSubject<Void>()
    let accountSelected = PublishSubject<AccountEntity>()
    
    //outputs
    let sections: Driver<[SectionModel]>
    let screenTitle: Driver<String>

    init(service: AccountListService, context: AccountListModule.ModuleOutput.Context) {
        self.service = service
        
        let accountListResponse = didLoad
            .flatMapLatest(service.getAccountList)
            .share()
        
        accountSelected
            .map { $0.accountNumber }
            .bind(to: context.showDetail)
            .disposed(by: disposeBag)
        
        screenTitle = didLoad
            .mapTo("Transparentní účty")
            .asDriver(onErrorDriveWith: .empty())
    
        sections = accountListResponse
            .elements()
            .map { [SectionModel(model: .default, items: $0.accounts)] }
            .asDriver(onErrorDriveWith: .empty())
    }
    
}


extension AccountEntity: IdentifiableType {

    var identity: String {
        return accountNumber
    }
    
}
