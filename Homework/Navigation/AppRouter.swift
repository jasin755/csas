//
//  AppRouter.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 11/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation
import Swinject
import RxSwift

class AppRouter {
    
    enum AppRouterOptions {
        case push
        case modal(presentationStyle: UIModalPresentationStyle)
        case set(isAnimated: Bool)
    }
    
    var topViewController: UIViewController? {
        
        let keyWindow = UIApplication.shared.connectedScenes
            .filter{ $0.activationState == .foregroundActive }
            .map{ $0 as? UIWindowScene }
            .compactMap{ $0 }
            .first?.windows
            .filter{ $0.isKeyWindow}
            .first
        
        var top = keyWindow?.rootViewController
        while true {
            if let presented = top?.presentedViewController {
                top = presented
            } else if let nav = top as? UINavigationController {
                top = nav.visibleViewController
            } else {
                break
            }
        }
        return top
    }
    
    let disposeBag = DisposeBag()
    let resolver: Resolver
    
    init(resolver: Resolver) {
        self.resolver = resolver
    }
        
    func show(controller: UIViewController, options: AppRouterOptions) {
        DispatchQueue.mainSyncSafe {
            switch options {
            case .push:
                AppDelegate.rootNavigationController.presentedViewController?.dismiss(animated: true, completion: nil)
                AppDelegate.rootNavigationController.pushViewController(controller, animated: true)
            case .modal(let presentationStyle):
                controller.modalPresentationStyle = presentationStyle
                AppDelegate.rootNavigationController.present(controller, animated: true, completion: nil)
            case .set(let isAnimated):
                AppDelegate.rootNavigationController.setViewControllers([controller], animated: isAnimated)
            }
        }
    }
    
}
