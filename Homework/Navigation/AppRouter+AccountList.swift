//
//  AppRouter+AccountList.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 11/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

extension AppRouter {
    
    func getAccountListView() -> UIViewController {
        let accountListModule = resolver.resolve(AccountListModule.self)!
        let moduleOutput =  accountListModule.show()
        let viewController = moduleOutput.viewController
        
        moduleOutput
            .showDetail
            .drive(onNext: { (accountNumber) in
                self.showAccountDetail(accountNumber: accountNumber)
            })
            .disposed(by: disposeBag)
        
        return viewController
    }
    
    func showAccountList() {
        show(controller: getAccountListView(), options: .push)
    }
    
}
