//
//  AppRouter+AccountDetail.swift
//  Homework
//
//  Created by Nikolaj Pogněrebko on 12/07/2020.
//  Copyright © 2020 Nikolaj Pogněrebko. All rights reserved.
//

import Foundation

extension AppRouter {
    
    func showAccountDetail(accountNumber: String) {
        let accountDetailModule = resolver.resolve(AccountDetailModule.self)!
        let moduleOutput = accountDetailModule.show(accountNumber: accountNumber)
        
        show(controller: moduleOutput.viewController, options: .push)
        
        
    }
    
}
